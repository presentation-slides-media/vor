#!/usr/bin/env bash

set -e

script_dir=$(dirname "$0")
dist_dir=${script_dir}/../public
src_dir=${script_dir}/../src
etc_dir=etc

mkdir -p ${dist_dir}

src_files=$(find -L ${src_dir} -maxdepth 1 -type f -regex ".*\.\(svg\|png\|jpg\)" | sort)
resize_sizes="800x800 600x600 450x450 250x250 200x200 100x100"

for src_file in ${src_files}; do
  src_file_basename=$(basename -- "${src_file}")
  src_file_filename="${src_file_basename%.*}"
  src_file_extension="${src_file_basename##*.}"
  echo ${src_file}

  if [ ${src_file_extension} == "svg" ]; then
    formats="jpg png"
    resize_extensions="jpg png"
  else if [ ${src_file_extension} == "png" ]; then
    formats="jpg svg"
    resize_extensions="png jpg"
  else if [ ${src_file_extension} == "jpg" ]; then
    formats="svg png"
    resize_extensions="jpg png"
  fi
  fi
  fi

  # convert
  for format in ${formats}; do
    echo "  ${format} convert"
    echo "convert \"${src_file}\" \"${dist_dir}/${src_file_filename}.${format}\""
    convert "${src_file}" "${dist_dir}/${src_file_filename}.${format}"
  done

  # resize
  for resize_extension in ${resize_extensions}; do
    for size in ${resize_sizes}; do
      echo "  ${resize_extension} resize ${size}"
      convert -resize ${size} "${src_file}" "${dist_dir}/${src_file_filename}-${size}.${resize_extension}"      
    done
  done
done

rsync -aH ${src_dir}/ ${dist_dir}

(
  cd ${dist_dir}
  echo "<html><body><h1>Directory listing:</h1>" > "index.html"
  find -exec echo "<a href='{}'>{}</a><br/>" \; >> "index.html"
  echo "</body></html>" >> "index.html"
)

